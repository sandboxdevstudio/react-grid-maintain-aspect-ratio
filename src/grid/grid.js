import React from "react";

import GridCellContainer from "./grid-cell-container";

import styled from "styled-components";

import "./grid.css";

class Grid extends React.Component {
  constructor(props) {
    super(props);

    this.gridRef = React.createRef();

    this.buildGridCells = this.buildGridCells.bind(this);

    this.calculateGridWidth = this.calculateGridWidth.bind(this);
    this.setGridWidth = this.setGridWidth.bind(this);
  }

  render() {
    let gridWidth = 0;
    let gridCells = null;

    if (this.props.gridContainerMounted) {
      gridWidth = this.calculateGridWidth() + "px";
      gridCells = this.buildGridCells();
    }

    // Use styled components to dynamically set the grid column template and the initial width.
    const Grid = styled.div`
      grid-template-columns: repeat(${this.props.columnCount}, 1fr);
      width: ${gridWidth};
    `;

    return (
      <Grid id="grid" ref={this.gridRef}>
        {gridCells}
      </Grid>
    );
  }

  buildGridCells() {
    const gridCells = [];

    for (let rowIndex = 0; rowIndex < this.props.rowCount; rowIndex++) {
      for (
        let columnIndex = 0;
        columnIndex < this.props.columnCount;
        columnIndex++
      ) {
        gridCells.push(
          <GridCellContainer
            rowIndex={rowIndex}
            columnIndex={columnIndex}
            connectGridCell={this.props.connectGridCell}
            key={`grid-cell-container-${rowIndex}-${columnIndex}`}
          />
        );
      }
    }

    return gridCells;
  }

  componentDidMount() {
    // Listen for window resize events.
    window.addEventListener("resize", this.setGridWidth);
  }

  componentWillUnmount() {
    // Remove the window resize event listener when the grid component unmounts.
    window.removeEventListener("resize", this.setGridWidth);
  }

  calculateGridWidth() {
    // Get the grid container height and width.
    const gridContainerDimensions = this.props.getGridContainerDimensions();
    const gridContainerHeight = gridContainerDimensions.height;
    const gridContainerWidth = gridContainerDimensions.width;

    // Calculate the grid width with regard to the grid dimensions and aspect ratio.
    const rowCount = this.props.rowCount;
    const columnCount = this.props.columnCount;
    let gridWidth = (gridContainerHeight / rowCount) * columnCount;

    // Reduce the grid width if the calculated value exceeds the grid container width.
    if (gridWidth > gridContainerWidth) {
      gridWidth = gridContainerWidth;
    }

    return gridWidth;
  }

  setGridWidth() {
    this.gridRef.current.style.width = this.calculateGridWidth() + "px";
  }
}

export default Grid;

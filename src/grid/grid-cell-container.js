import React from "react";

import GridCell from "./grid-cell";

function GridCellContainer(props) {
  const rowIndex = props.rowIndex;
  const columnIndex = props.columnIndex;
  const gridCellContainerId = `grid-cell-container-${rowIndex}-${columnIndex}`;

  return (
    <div id={gridCellContainerId} className="grid-cell-container">
      <GridCell
        rowIndex={rowIndex}
        columnIndex={columnIndex}
        connectGridCell={props.connectGridCell}
      />
    </div>
  );
}

export default GridCellContainer;

import React from "react";

import Grid from "./grid";

class GridContainer extends React.Component {
  constructor(props) {
    super(props);

    this.gridContainerRef = React.createRef();

    this.state = {
      gridContainerMounted: false
    };

    this.getGridContainerDimensions = this.getGridContainerDimensions.bind(
      this
    );
  }

  render() {
    return (
      <div id="grid-container" ref={this.gridContainerRef}>
        <Grid
          rowCount={this.props.rowCount}
          columnCount={this.props.columnCount}
          connectGridCell={this.props.connectGridCell}
          gridContainerMounted={this.state.gridContainerMounted}
          getGridContainerDimensions={this.getGridContainerDimensions}
        />
      </div>
    );
  }

  componentDidMount() {
    this.setState({
      gridContainerMounted: true
    });
  }

  getGridContainerDimensions() {
    const gridContainerDimensions = {
      height: 0,
      width: 0
    };

    if (this.gridContainerRef.current) {
      let gridContainerBoundingBox = this.gridContainerRef.current.getBoundingClientRect();

      gridContainerDimensions.height = gridContainerBoundingBox.height;
      gridContainerDimensions.width = gridContainerBoundingBox.width;
    }

    return gridContainerDimensions;
  }
}

export default GridContainer;

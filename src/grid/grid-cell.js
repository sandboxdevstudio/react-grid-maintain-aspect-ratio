import React from "react";

class GridCell extends React.Component {
  constructor(props) {
    super(props);

    this.setGridCellContent = this.setGridCellContent.bind(this);

    this.state = {
      gridCellContent: null
    };
  }

  render() {
    const rowIndex = this.props.rowIndex;
    const columnIndex = this.props.columnIndex;
    const gridCellId = `grid-cell-${rowIndex}-${columnIndex}`;

    return (
      <div id={gridCellId} className="grid-cell">
        {this.state.gridCellContent}
      </div>
    );
  }

  componentDidMount() {
    this.props.connectGridCell(
      this.props.rowIndex,
      this.props.columnIndex,
      this.setGridCellContent
    );
  }

  setGridCellContent(gridCellContent) {
    this.setState({
      gridCellContent
    });
  }
}

export default GridCell;

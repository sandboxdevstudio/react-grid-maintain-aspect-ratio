import React from "react";
import ReactDOM from "react-dom";

import Base from "./base/base";

import "./index.css";

ReactDOM.render(<Base />, document.getElementById("root"));

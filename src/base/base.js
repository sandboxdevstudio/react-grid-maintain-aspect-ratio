import React, { useState } from "react";

import GridContainer from "../grid/grid-container";

import "./base.css";

function Base() {
  const [rowCount, setRowCount] = useState(10);
  const [columnCount, setColumnCount] = useState(10);

  const connectGridCell = (rowIndex, columnIndex, setGridCellContent) => {
    console.log(`Connected grid cell [${rowIndex}, ${columnIndex}].`);

    // https://www.paulirish.com/2009/random-hex-color-code-snippets/
    const randomHexColorCode = `#${Math.floor(
      Math.random() * 16777215
    ).toString(16)}`;

    setGridCellContent(
      <div
        style={{
          height: "100%",
          width: "100%",
          backgroundColor: randomHexColorCode
        }}
      />
    );
  };

  const changeGridDimensions = () => {
    const rowCount = Math.floor(Math.random() * 16) + 1;
    setRowCount(rowCount);

    const columnCount = Math.floor(Math.random() * 16) + 1;
    setColumnCount(columnCount);
  };

  return (
    <div id="base">
      <header>
        <h1>
          A responsive grid that maintains aspect ratio, built using React.
        </h1>
        <p>
          The grid will fill the entire space available while maintaining a 1:1
          aspect ratio, and without exceeding the bounds of the container. The
          grid can be of arbitrary dimensions, and was developed for a
          grid-based game.
        </p>
        <p>
          Current grid dimensions: [{rowCount}, {columnCount}]
        </p>
        <button onClick={() => changeGridDimensions()}>
          Change Grid Dimensions
        </button>
      </header>
      <GridContainer
        rowCount={rowCount}
        columnCount={columnCount}
        connectGridCell={connectGridCell}
      />
      <footer>Source code available here.</footer>
    </div>
  );
}

export default Base;
